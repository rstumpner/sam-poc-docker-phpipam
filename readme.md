# SAM POC with Docker and PHPIPAM
This is a Gitlab Repository to do an easy Demo or POC Setup of the  PHPIPAM ( https://github.com/phpipam/phpipam ) Open Source Software.

To do the POC on your Local Machine the following Steps are necessary:
* Prerequisites:
  * Docker
  * Docker-Compose
  * Browser
* Install Docker
* Install Docker-Compose
* Clone this Gitlab Repository
  * git clone https://gitlab.com/rstumpner/sam-poc-docker-phpipam
* Change Directory to the Repository
  * cd sam-poc-docker-phpipam
* Fire up docker-compose up
  * docker-compose up
* Debug Issue #1
  * docker-compose ps
    * Get Container ID
  * docker exec -it 86a00536725a bash
  * sed -i "s/localhost/mariadb/" config.php
  * sed -i "s/phpipamadmin/poc-phpipam/" config.php  
* Start the Browser and type in http://localhost
